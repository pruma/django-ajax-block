from django.conf.urls import patterns, url

urlpatterns = patterns('ajax_block.views',
    url(r'^ajax_refresh/$', 'ajax_refresh', name='refresh_ajax_block')
)
