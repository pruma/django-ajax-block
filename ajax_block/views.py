# -*- coding: utf-8 -*-
# author: Semen Pupkov (semen.pupkov@gmail.com)
from clevercms.lib.decorators import ajax_request
from ajax_block.models import AjaxBlock


@ajax_request
def ajax_refresh(request):
    current_id = request.GET.get('cid')

    block = AjaxBlock.objects.filter(active=1).exclude(id__in=current_id).order_by('?')[0]
    return {
            'block_name': block.name,
            'block_text': block.text,
            'block_id': block.id
            }

