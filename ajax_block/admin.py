# -*- coding: utf-8 -*-
# Author: Semen Pupkov (semen.pupkov@gmail.com)

from django.contrib import admin
from ajax_block.models import AjaxBlock


class AjaxBlockAdmin(admin.ModelAdmin):
    """docstring for AjaxBlockAdmin"""
    list_display = ('text', 'name', 'active', 'created_at', 'updated_at',)

admin.site.register(AjaxBlock, AjaxBlockAdmin)
