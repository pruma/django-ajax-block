# -*- coding: utf-8 -*-
from django import template
from ajax_block.models import AjaxBlock

register = template.Library()


@register.inclusion_tag('ajax_block/block.html', takes_context = True)
def ajax_block(context):
    block = AjaxBlock.objects.filter(active=1).order_by('?')[0]

    return {
            'block':block,
            'request': context['request']
    }

