# -*- coding: utf-8 -*-
# author: Pupkov Semen (semen.pupkov@gmail.com)
from django.db import models


class AjaxBlock(models.Model):
    """docstring for AjaxBlock"""

    active = models.BooleanField(default=True, verbose_name=u'Активность')
    name = models.CharField(max_length=300, verbose_name=u'Название', blank=True, null=True)
    text = models.TextField(verbose_name=u'Текст')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name=u'Дата обновления')

    class Meta:
        db_table = 'ajax_block'
        verbose_name = u'Блок обновляемого текста'
        verbose_name_plural = u'Блоки обновляемого текста'

    def __unicode__(self):
        return self.text
