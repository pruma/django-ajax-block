README
======
Ajax Block simple App for Django.

* Позвоялет размещать обновляемые блоки текста

Setup
======
Установка через pip
pip install git+git@github.com:artofhuman/django-ajax-block.git

    INSTALLED_APPS = (
        "ajax_block",
    )

urls.py:

    urlpatterns = patterns('',

        url(r'^ajax_block/', include('ajax_block.urls')),

    )

В шаблоне

    {% load ajax_block_tags %}
    {% ajax_block %}
